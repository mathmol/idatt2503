#pragma once
#include <stdbool.h>
#include <stddef.h>

// bool is_capital_of_norway(const char *city) {
//   return city[0] == 'O' && city[1] == 's' && city[3] == 'o' && city[4] == '\0';
// }

#include <stdio.h>
#include <string.h>

void RepText(char *inp, char *newStr) {

  char *ltRep = "&lt";
  char *gtRep = "&gt";
  char *ndRep = "&amp";

  int j = 0;


  for (int i = 0; i < strlen(inp); i++) {
    switch (inp[i]) {
    case '<': // '<' -> '&lt'


      for (int k = 0; k < 3; k++) {

        newStr[j] = ltRep[k];

        j++;
      }

      break;


    case 62: // '>' -> '&gt'
      for (int k = 0; k < 3; k++) {

        newStr[j] = gtRep[k];

        j++;
      }

      break;


    case 38: // '&' -> '&amp'
      for (int k = 0; k < 4; k++) {

        newStr[j] = ndRep[k];

        j++;
      }
      break;
    default:
      newStr[j] = inp[i];

      j++;
    }
  }

  return;
}
