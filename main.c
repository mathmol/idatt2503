#include "utility.h"
#include <stdio.h>
#include <string.h>

int main()
{
  char *inp = "A & test < string >!";
  char replaced[strlen(inp) * 4];
  RepText(inp, replaced);
  printf("Replaced string %s", replaced);
  return 0;
}
